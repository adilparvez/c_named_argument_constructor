#include "list.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>


bool equals(void *x, void *y) {
  return *((int *)x) == *((int *)y);
}


int compare(void *x, void *y) {
  return *((int *)x) - *((int *)y);
}

void destroy(void *x) {
  // Do nothing.
}

void into_print_buffer(list *list, void *element) {
  sprintf(list->print_buffer, "%d", *((int *)element));
}

void function(void *x) {
  int d = 2 * (*(int *)x);
  memcpy(x, &d, sizeof(int));
}

bool is_eight(void *element) {
  return *((int *)element) == 8;
}

bool even(void *element) {
  int num = *((int *)element);
  return num % 2 == 0;
}

int main(int argc, char **argv) {
  list l;
  list_create(&l, sizeof(int), .destructor=destroy, .stringifier=into_print_buffer, .comparator=compare, .equals=equals);

  for (int i = -4; i < 4; i++) {
    list_push_back(&l, &i);
  }

  list_allocate_print_buffer(&l, 20);
  list_print(&l, stdout, false);
  list_reverse(&l);
  list_print(&l, stdout, false);
  list_for_each(&l, function);
  list_print(&l, stdout, false);
  int i = 1;
  list_print(&l, stdout, false);
  list_remove_every(&l, &i);
  list_remove_duplicates(&l);
  list_print(&l, stdout, false);
  list_for_each(&l, function);
  list_print(&l, stdout, false);
  list_reverse(&l);
  list_print(&l, stdout, false);
  list_remove_at(&l, 0);
  list_print(&l, stdout, true);
  list_remove_all(&l);
  list_print(&l, stdout, false);
  list_destroy(&l);
  return 0;
}
