#ifndef list_h
#define list_h

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>


typedef struct list_node {
  struct list_node *previous;
  struct list_node *next;
  void *element;
} list_node;

typedef void (*destructor)(void *element);
typedef int (*comparator)(void *element1, void *element2);
typedef bool (*equality_test)(void *element1, void *element2);

struct list;

typedef void (*stringifier)(struct list *list, void *element);

typedef struct list {
  list_node *first;
  list_node *last;
  uint32_t size;
  uint32_t print_buffer_width;
  char *print_buffer;
  size_t sizeof_element;
  destructor destroy;
  comparator compare;
  equality_test equals;
  stringifier into_print_buffer;
} list;

typedef bool (*predicate)(void *element);
typedef void (*element_function)(void *element);

typedef struct {
  list *list;
  size_t sizeof_element;
  destructor destructor;
  comparator comparator;
  equality_test equals;
  stringifier stringifier;
} list_create_parameters;


// list_create(
//   , .list=
//   , .sizeof_element=
//   , .destuctor=
//   , .comparator=
//   , .equals=
//   , .stringifier=
// );

void list_create_base(list *list, size_t sizeof_element, destructor destroy, comparator compare, equality_test equals, stringifier into_print_buffer);
void list_create_named(list_create_parameters params);
#define list_create(...) list_create_named((list_create_parameters){__VA_ARGS__});

void list_destroy(list *list);
uint32_t size(list *list);
bool list_empty(list *list);
void list_push_front(list *list, void *element);
void list_pop_front(list *list, void *element);
void list_peek_front(list *list, void *element);
void list_push_back(list *list, void *element);
void list_pop_back(list *list, void *element);
void list_peek_back(list *list, void *element);
void list_get(list *list, uint32_t index, void *element);
list_node *list_remove(list *list, void *element);
bool list_remove_every(list *list, void *element);
void list_remove_at(list *list, uint32_t index);
bool list_remove_if(list *list, predicate drop_condition);
bool list_remove_duplicates(list *list);
void list_remove_all(list *list);
void list_reverse(list *list);
void list_for_each(list *list, element_function function);
void list_allocate_print_buffer(list *list, uint32_t buffer_size);
void list_free_print_buffer(list *list);
void list_print(list *list, FILE *stream, bool one_element_per_line);

#endif
