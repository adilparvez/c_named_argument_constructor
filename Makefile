CC = gcc
CFLAGS = -Wall -std=c99
OBJS = test.o list.o

test: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o test

test.o: test.c
	$(CC) $(CFLAGS) -c test.c -o test.o

list.o: list.c list.h
	$(CC) $(CFLAGS) -c  list.c -o list.o

clean:
	rm *.o test
