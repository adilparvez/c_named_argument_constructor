#include "list.h"

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>


void list_create_base(list *list, size_t sizeof_element, destructor destroy, comparator compare, equality_test equals, stringifier into_print_buffer) {
  assert(sizeof_element > 0);

  list->first = NULL;
  list->last = NULL;
  list->size = 0;
  list->print_buffer_width = 0;
  list->print_buffer = NULL;

  list->sizeof_element = sizeof_element;
  list->destroy = destroy;
  list->compare = compare;
  list->equals = equals;
  list->into_print_buffer = into_print_buffer;
}

void list_create_named(list_create_parameters params) {
  assert(params.list != NULL);
  assert(params.sizeof_element > 0);

  list *list = params.list;
  size_t sizeof_element = params.sizeof_element;
  destructor destroy = params.destructor ? params.destructor : NULL;
  comparator compare = params.comparator ? params.comparator : NULL;
  equality_test equals = params.equals ? params.equals : NULL;
  stringifier into_print_buffer = params.stringifier ? params.stringifier : NULL;
  list_create_base(list, sizeof_element, destroy, compare, equals, into_print_buffer);
}

void list_destroy(list *list) {
  assert(list != NULL);
  assert(list->destroy != NULL);

  list_node *runner;
  while (list->first) {
    runner = list->first;
    list->first = runner->next;
    list->destroy(runner->element);
    free(runner->element);
    free(runner);
  }

  if (list->print_buffer) {
    list_free_print_buffer(list);
  }
}

uint32_t size(list *list) {
  assert(list != NULL);
  return list->size;
}

bool list_empty(list *list) {
  assert(list != NULL);
  return list->size == 0;
}

void list_push_front(list *list, void *element) {
  assert(list != NULL);
  assert(element != NULL);

  list_node *new_node = (list_node *)malloc(sizeof(list_node));
  new_node->element = malloc(list->sizeof_element);
  memcpy(new_node->element, element, list->sizeof_element);

  if (!list->first) {
    list->first = new_node;
    list->last = new_node;
  } else {
    new_node->next = list->first;
    list->first->previous = new_node;
    list->first = new_node;
  }

  ++(list->size);
}

void list_pop_front(list *list, void *element) {
  assert(list != NULL);
  assert(element != NULL);

  list_peek_front(list, element);
  list_remove_at(list, 0);
}

void list_peek_front(list *list, void *element) {
  assert(list != NULL);
  assert(element != NULL);
  assert(list->first != NULL);

  memcpy(element, list->first->element, list->sizeof_element);
}

void list_push_back(list *list, void *element) {
  assert(list != NULL);
  assert(element != NULL);

  list_node *new_node = (list_node *)malloc(sizeof(list_node));
  new_node->element = malloc(list->sizeof_element);
  memcpy(new_node->element, element, list->sizeof_element);
  new_node->next = NULL;
  new_node->previous = NULL;

  if (!list->last) {
    list->first = new_node;
    list->last = new_node;
  } else {
    list->last->next = new_node;
    new_node->previous = list->last;
    list->last = new_node;
  }

  ++(list->size);
}

void list_pop_back(list *list, void *element) {
  assert(list != NULL);
  assert(element != NULL);

  list_peek_back(list, element);
  list_remove_at(list, list->size - 1);
}

void list_peek_back(list *list, void *element) { 
  assert(list != NULL);
  assert(element != NULL);
  assert(list->last != NULL);

  memcpy(element, list->last->element, list->sizeof_element);
}

void list_get(list *list, uint32_t index, void *element) {
  assert(list != NULL);
  assert(0 <= index && index < list->size);
  assert(element != NULL);

  list_node *runner = list->first;
  while (index > 0) {
    runner = runner->next;
    --index;
  }

  memcpy(element, runner->element, list->sizeof_element);
}

static list_node *list_remove_node(list *list, list_node *to_remove) {
  assert(list != NULL);
  assert(list->destroy != NULL);
  assert(to_remove != NULL);

  list_node *next = to_remove->next;

  if (to_remove == list->first) {
    list->first = to_remove->next;
    if (list->first) {
      list->first->previous = NULL;
    }
  } else if (to_remove == list->last) {
    list->last = to_remove->previous;
    if (list->last) {
      list->last->next = NULL;
    }
  } else {
    to_remove->previous->next = to_remove->next;
    to_remove->next->previous = to_remove->previous;
  }

  list->destroy(to_remove->element);
  free(to_remove->element);
  free(to_remove);
  
  --(list->size);

  return next;
}

list_node *list_remove(list *list, void *element) {
  assert(list != NULL);
  assert(list->destroy != NULL);
  assert(list->equals != NULL);
  assert(element != NULL);

  list_node *runner = list->first;
  while (runner != NULL) {
    if (list->equals(runner->element, element)) {
      return list_remove_node(list, runner);
    }
    runner = runner->next;
  }

  return NULL;
}

bool list_remove_every(list *list, void *element) {
  assert(list != NULL);
  assert(list->destroy != NULL);
  assert(list->equals != NULL);
  assert(element != NULL);

  bool something_removed = false;

  list_node *runner = list->first;
  while (runner != NULL) {
    if (list->equals(runner->element, element)) {
      runner = list_remove_node(list, runner);
      something_removed = true;
    } else {
      runner = runner->next;
    }
  }

  return something_removed;
}

void list_remove_at(list *list, uint32_t index) {
  assert(list != NULL);
  assert(list->destroy != NULL);
  assert(list->equals != NULL);
  assert(0 <= index && index < list->size);

  list_node *runner = list->first;
  while (index > 0) {
    runner = runner->next;
    --index;
  }

  list_remove_node(list, runner);
}

bool list_remove_if(list *list, predicate drop_condition) {
  assert(list != NULL);
  assert(list->destroy != NULL);
  assert(list->equals != NULL);
  assert(drop_condition != NULL);

  bool something_removed = false;

  list_node *runner = list->first;
  while (runner != NULL) {
    if (drop_condition(runner->element)) {
      runner = list_remove_node(list, runner);
      something_removed = true;
    } else {
      runner = runner->next;
    }
  }

  return something_removed;
}

bool list_remove_duplicates(list *list) {
  assert(list != NULL);
  assert(list->destroy != NULL);
  assert(list->equals != NULL);

  bool something_removed = false;

  list_node *primary_runner = list->first;
  list_node *secondary_runner = NULL;
  list_node *to_remove = NULL;
  while (primary_runner != NULL && primary_runner->next != NULL) {
    secondary_runner = primary_runner->next;
    while (secondary_runner != NULL) {
      if (list->equals(primary_runner->element, secondary_runner->element)) {
        to_remove = secondary_runner;
        secondary_runner = secondary_runner->next;
        list_remove_node(list, to_remove);
        something_removed = true;
      } else {
        secondary_runner = secondary_runner->next;
      }
    }
    primary_runner = primary_runner->next;
  }

  return something_removed;
}

void list_remove_all(list *list) {
  assert(list != NULL);
  assert(list->destroy != NULL);

  list_node *runner = list->first;
  while (runner != NULL) {
    runner = list_remove_node(list, runner);
  }
}

static *list_node list_clone_node(list *list, list_node *to_clone) {
  assert(list != NULL);
  assert(to_clone != NULL);

  list_node *new_node = (list_node *)malloc(sizeof(list_node));
  new_node->element = malloc(list->sizeof_element);
  memcpy(new_node->element, to_clone->element, list->sizeof_element);

  return new_node;
}

void list_reverse(list *list) {
  assert(list != NULL);

  list_node *first = list->first;
  list_node *runner = first;
  list_node *runner_ahead = NULL;
  while (runner->next != NULL) {
    runner_ahead = runner->next;
    runner->next = runner->previous;
    runner->previous = runner_ahead;
    runner = runner_ahead;
  }
  runner->next = runner->previous;
  list->first = runner_ahead;
  list->last = first;
  list->last->next = NULL;
}

void list_for_each(list *list, element_function function) {
  assert(list != NULL);

  list_node *runner = list->first;
  while (runner != NULL) {
    function(runner->element);
    runner = runner->next;
  }
}

void list_allocate_print_buffer(list *list, uint32_t buffer_size) {
  assert(list != NULL);
  assert(list->print_buffer == NULL);
  assert(buffer_size > 0);

  list->print_buffer_width = buffer_size;
  list->print_buffer = (char *)malloc(sizeof(char) * buffer_size);
}

static void list_clear_print_buffer(list *list) {
  list->print_buffer[0] = '\0';
}

void list_free_print_buffer(list *list) {
  assert(list != NULL);
  assert(list->print_buffer != NULL);

  free(list->print_buffer);
  list->print_buffer = NULL;
}

void list_print(list *list, FILE *stream , bool one_element_per_line) {
  assert(list != NULL);
  assert(list->print_buffer != NULL);
  assert(list->into_print_buffer != NULL);

  if (one_element_per_line) {
    fprintf(stream, "<LIST SIZE=%d>\n", list->size);
  } else {
    fputs("[", stream); 
  }

  if (list->size == 0) {
    one_element_per_line ? fputs("<LIST>\n", stream): fputs("]\n" , stream);
    return;
  }
  
  list_node *runner = list->first;
  int index = 0;
  while (runner->next != NULL) {
    list->into_print_buffer(list, runner->element);
    if (one_element_per_line) {
      fprintf(stream, "[%d]:\n%s\n", index, list->print_buffer);
    } else {
      fprintf(stream, "%s, ", list->print_buffer);
    }
    list_clear_print_buffer(list);
    ++index;
    runner = runner->next;
  }

  list->into_print_buffer(list, runner->element);
  if (one_element_per_line) {
    fprintf(stream, "[%d]:\n%s\n<LIST>\n", index, list->print_buffer);
  } else {
    fprintf(stream, "%s]\n", list->print_buffer);
  }
  list_clear_print_buffer(list);
}
